/*

Питання 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

Відповідь 1. Екрануванння це додавання спецсимволів, для коректної інтерпритації значень рядків або символів. 
Питання 2. Які засоби оголошення функцій ви знаєте?

Відповідь 2. Є три способи оголошення: 

                f declaration - оголошується в основному потокі коду, має ім`я та параметри, може бути викликана до її оголошення; 
                f expression - без ім`я (анонімна), її присвоюють до змінної, вона не займає місце в пам`яті, не можна викликати до оголошення;
                f стрілочна - як різновид анонімної, замість function використовується =>

Питання 3. Що таке hoisting, як він працює для змінних та функцій?

Відповідь 3. hoisting це коли функцію або змінну можна використати до оголошення. 

*/

"use strict"; //strict mode is a way to introduce better error-checking into your code 

function createNewUser() {
    
    const newUser = {
        getLogin: function(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
          
        getAge: function(){
            const CurrentDate = new Date();  
            return CurrentDate.getFullYear() - this.birthDay.getFullYear();
        },
          
        getPassword: function(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.getFullYear();  
        },

        setFirstName(_firstName) {
            this.firstName = _firstName;
        },
          
        setLastName(_lastName) {
            this.lastName = _lastName;
        },
          
        setBirthDay(_birthDay) {
            this.birthDay = _birthDay;
        },
    };
    
    Object.defineProperties(newUser, {
        firstName: {
            value: prompt("Enter first name:", ""),
            writable: false, 
            configurable: true, 
        },
  
        lastName: {
            value: prompt("Enter last name:", ""),
            writable: false,
            configurable: true,
        },

        birthDay: {
            value: new Date(prompt("Enter your date of birth:(dd.mm.yyyy)", "").split('.').reverse()),
            writable: false,
            configurable: true,
        },
    });
  
    return newUser;
}

let user = createNewUser();

console.log('ви ввели ім`я: ' + user.firstName);
console.log('ви ввели прізвище: ' + user.lastName);
console.log('ви ввели таку дату народження: ' + user.birthDay);

console.log(user);
console.log('для вас створено логін: ' + user.getLogin());
console.log('для вас створено пароль: ' + user.getPassword());
console.log('вам цього року: ' + user.getAge());
 